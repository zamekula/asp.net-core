﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServerParking.Shared
{
	public enum CarType
	{
		Passenger,
		Truck,
		Bus,
		Motorcycle
	}
}
