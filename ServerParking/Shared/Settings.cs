﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServerParking.Shared
{
	public static class Settings
	{
		public static readonly int _logTimeout = 60;
		public static readonly int _timeout = 5;
		public static readonly int _balance = 0;
		public static readonly int _parkingSpace = 10;
		public static readonly IReadOnlyDictionary<string, double> _bills;
		public static readonly double _fine = 2.5;
		public static readonly string _logPath = "Transactions.log";

		static Settings()
		{
			var cars = Enum.GetNames(typeof(CarType));
			_bills = new Dictionary<string, double>()
			{
				{cars[0], 2},
				{cars[1], 5},
				{cars[2], 3.5},
				{cars[3], 1}
			};
		}
	}
}
