﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerParking.Core;
using ServerParking.Services.Intefaces;

namespace ServerParking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarsController : Controller
    {
		ICarService service;

		public CarsController(ICarService service)
		{
			this.service = service;
		}

		[HttpGet]
		[Route("cars")]
		public JsonResult GetCars()
		{
			try
			{
				return Json(service.GetCars());
			}
			catch(Exception ex)
			{
				return Json(BadRequest(ex.Message));
			}
		}

		[HttpGet]
		[Route("car/{id}")]
		public JsonResult GetCar(string id)
		{
			try
			{
				return Json(service.GetCar(id));
			}
			catch (Exception ex)
			{
				return Json(BadRequest(ex.Message));
			}

		}

		[HttpPost]
		[Route("car")]
		public JsonResult AddCar([FromBody] string jsonCar)
		{
			try
			{
				var car = Newtonsoft.Json.JsonConvert.DeserializeObject<Car>(jsonCar);
				return Json(service.AddCar(car.Type, car.Balance));
			}
			catch (Exception ex)
			{
				return Json(BadRequest(ex.Message));
			}
		}

		[HttpPost]
		[Route("balance")]
		public JsonResult AddBalance([FromBody] string jsonCar)
		{
			try
			{
				var car = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(jsonCar);
				return Json(service.AddBalance((string)car.ID, (double)car.Balance));
			}
			catch (Exception ex)
			{
				return Json(BadRequest(ex.Message));
			}
		}

		[HttpDelete]
		[Route("car/{id}")]
		public JsonResult DeleteCar(string id)
		{
			try
			{
				return Json(service.DeleteCar(id));
			}
			catch (Exception ex)
			{
				return Json(BadRequest(ex.Message));
			}
		}
	}
}