﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerParking.Services.Intefaces;

namespace ServerParking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionsController : Controller
    {
		ITransactionService service;

		public TransactionsController(ITransactionService service)
		{
			this.service = service;
		}

		[HttpGet]
		[Route("transactions")]
		public JsonResult GetTransactions()
		{
			try
			{
				return Json(service.GetTransactions());
			}
			catch (Exception ex)
			{
				return Json(BadRequest(ex.Message));
			}
		}

		[HttpGet]
		[Route("log")]
		public JsonResult GetLog()
		{
			try
			{
				return Json(service.GetLog());
			}
			catch (Exception ex)
			{
				return Json(BadRequest(ex.Message));
			}
		}
		
	}
}