﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServerParking.Services.Intefaces;

namespace ServerParking.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : Controller
    {
		IParkingService service;

		public ParkingController(IParkingService service)
		{
			this.service = service;
		}

		[HttpGet]
		[Route("balance")]
		public JsonResult GetBalance()
		{
			try
			{
				return Json(service.GetBalance());
			}
			catch (Exception ex)
			{
				return Json(BadRequest(ex.Message));
			}
		}

		[HttpGet]
		[Route("income")]
		public JsonResult GetIncome()
		{
			try
			{
				return Json(service.GetIncome());
			}
			catch (Exception ex)
			{
				return Json(BadRequest(ex.Message));
			}
		}

		[HttpGet]
		[Route("places")]
		public JsonResult GetPlaces()
		{
			try
			{
				return Json(service.GetPlaces());
			}
			catch (Exception ex)
			{
				return Json(BadRequest(ex.Message));
			}
		}

		
	}
}