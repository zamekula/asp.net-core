﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ServerParking.Core;
using ServerParking.Services.Intefaces;

namespace ServerParking.Services.Implementation
{
	public class ParkingService : IParkingService
	{
		Parking parking;
		public ParkingService(Parking parking)
		{
			this.parking = parking;
		}
		public double GetBalance()
		{
			return parking.Balance;
		}

		public double GetIncome()
		{
			return parking.ShowIncome(true);
		}

		public string GetPlaces()
		{
			return $"Total: {parking.TotalPlaces}\n" +
				   $"Free: {parking.FreePlaces}\n" +
				   $"Occupied: {parking.OccupiedPlaces}";
		}
	}
}
