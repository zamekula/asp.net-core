﻿using ServerParking.Core;
using ServerParking.Services.Intefaces;
using ServerParking.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerParking.Services.Implementation
{
	public class CarService : ICarService
	{
		Parking parking;
		public CarService(Parking parking)
		{
			this.parking = parking;
		}
		public bool AddBalance(string id, double money)
		{
			return parking.AddBalance(id.ToString(), money);
		}

		public string AddCar(CarType type, double balance)
		{
			parking.AddCar(type, balance, out string guid);
			return guid;
		}

		public bool DeleteCar(string id)
		{
			return parking.RemoveCar(id.ToString());
		}

		public Car GetCar(string id)
		{
			return parking.GetCar(id.ToString());
		}

		public IEnumerable<Car> GetCars()
		{
			return parking.Cars;
		}
	}
}
