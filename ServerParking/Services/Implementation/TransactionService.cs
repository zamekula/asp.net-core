﻿using ServerParking.Core;
using ServerParking.Services.Intefaces;
using ServerParking.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerParking.Services.Implementation
{
	public class TransactionService : ITransactionService
	{
		Parking parking;
		public TransactionService(Parking parking)
		{
			this.parking = parking;
		}

		public IEnumerable<Transaction> GetTransactions()
		{
			return parking.ShowTransactions();
		}

		string ITransactionService.GetLog()
		{
			return parking.ShowLog();
		}
	}
}
