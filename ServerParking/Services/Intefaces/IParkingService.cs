﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerParking.Services.Intefaces
{
	public interface IParkingService
	{
		double GetBalance();
		string GetPlaces();
		double GetIncome();
	}
}
