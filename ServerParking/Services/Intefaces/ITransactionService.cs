﻿using ServerParking.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerParking.Services.Intefaces
{
	public interface ITransactionService
	{
		IEnumerable<Transaction> GetTransactions();
		string GetLog();
	}
}
