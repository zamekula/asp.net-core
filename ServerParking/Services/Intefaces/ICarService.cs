﻿using ServerParking.Core;
using ServerParking.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerParking.Services.Intefaces
{
	public interface ICarService
	{
		string AddCar(CarType type, double balance);
		bool DeleteCar(string id);
		IEnumerable<Car> GetCars();
		Car GetCar(string id);
		bool AddBalance(string id, double money);
	}
}
