﻿using System;

namespace ClientParking.Models
{
	public class Transaction
	{
		public DateTime TransactionTime { get; set; }
		public string CarID { get;  set; }
		public double Tax { get;  set; }

		public override string ToString()
		{
			return $"Time: {TransactionTime}; Car: {CarID}; Tax: {Tax}\n";
		}
	}
}
