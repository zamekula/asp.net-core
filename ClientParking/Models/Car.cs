﻿using System;

namespace ClientParking.Models
{

	public class Car
	{
		public string ID { get; set; }
		public double Balance { get;  set; }
		public CarType Type { get; set; }
		public double Fine { get; set; }

		public override string ToString()
		{
			return $"CarId : {ID}; CarType: {Type.ToString()}; Balance: {Balance} Fine: {Fine}";
		}

	}
}
