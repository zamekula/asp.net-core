﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientParking.Models
{
	public enum CarType
	{
		Passenger,
		Truck,
		Bus,
		Motorcycle
	}
}
