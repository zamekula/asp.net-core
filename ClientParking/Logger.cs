﻿using System;
using System.IO;
using System.Text;

namespace ClientParking
{
	public static class Logger
	{
		public static void WriteException(string message, string fileName)
		{
			using (StreamWriter writer = new StreamWriter(fileName, true))
			{
				writer.WriteLine($"{DateTime.Now.ToLongTimeString()}\tERR\t{message}");
			}
		}

	}
}


