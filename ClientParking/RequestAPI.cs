﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace ClientParking
{
	public class RequestAPI
	{
		private static readonly Lazy<RequestAPI> lazy = new Lazy<RequestAPI>(() => new RequestAPI());
		public static RequestAPI Instanse { get { return lazy.Value; } }

		Newtonsoft.Json.JsonSerializerSettings settings = new Newtonsoft.Json.JsonSerializerSettings();
		const string baseAdress = "http://localhost:55593/api";

		private RequestAPI()
		{
			settings.DateFormatString = "YYYY-MM-DDTHH:mm:ss.FFFZ";
		}

		public List<T> GetItems<T>(string endpoint)
		{
			using (HttpClient client = new HttpClient())
			{
				var result = client.GetStringAsync(baseAdress + endpoint).Result;
				try
				{
					return Newtonsoft.Json.JsonConvert.DeserializeObject<List<T>>(result, settings);
				}
				catch (Exception e)
				{
					Console.WriteLine(Newtonsoft.Json.JsonConvert.DeserializeObject<ObjectResult>(result, settings).Value);
					return null;
				}
			}
		}

		public T GetItem<T>(string endpoint, string id)
		{
			using (HttpClient client = new HttpClient())
			{
				var result = client.GetStringAsync(baseAdress + endpoint + "/" + id).Result;
				try
				{
					return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(result, settings);
				}
				catch (Exception e)
				{
					Console.WriteLine(Newtonsoft.Json.JsonConvert.DeserializeObject<ObjectResult>(result, settings).Value);
					return default(T);
				}
			}
		}

		public T GetItem<T>(string endpoint)
		{
			using (HttpClient client = new HttpClient())
			{
				var result = client.GetStringAsync(baseAdress + endpoint).Result;
				try
				{
					return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(result, settings);
				}
				catch (Exception e)
				{
					Console.WriteLine(Newtonsoft.Json.JsonConvert.DeserializeObject<ObjectResult>(result, settings).Value);
					return default(T);
				}
			}
		}

		public T PostItem<T>(string endpoint, object parameters)
		{
			using (HttpClient client = new HttpClient())
			{
				var response = client.PostAsJsonAsync(baseAdress + endpoint, Newtonsoft.Json.JsonConvert.SerializeObject(parameters)).Result;
				var result = response.Content.ReadAsStringAsync().Result;
				try
				{
					return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(result, settings);
				}
				catch (Exception e)
				{
					Console.WriteLine(Newtonsoft.Json.JsonConvert.DeserializeObject<ObjectResult>(result, settings).Value);
					return default(T);
				}
			}
		}

		public T DeleteItem<T>(string endpoint, string id)
		{
			using (HttpClient client = new HttpClient())
			{
				HttpResponseMessage response = client.DeleteAsync(baseAdress + endpoint + "/" + id).Result;
				var result = response.Content.ReadAsStringAsync().Result;
				try
				{
					return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(result, settings);
				}
				catch(Exception e)
				{
					Console.WriteLine(Newtonsoft.Json.JsonConvert.DeserializeObject<ObjectResult>(result, settings).Value);
					return default(T);
				}
			}
		}


	}
}
