﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using ClientParking.Models;
using System.Threading.Tasks;

namespace ClientParking.IO
{
	public static class MenuMethods
	{
		#region FirstMenuLevelMethods

		public static void AddCar()
		{
			Console.Clear();
			Console.WriteLine("Select car type");
			Program.SelectSecondMenu();
		}

		public static void DeleteCar()
		{
			Console.Clear();
			Console.WriteLine("Enter car ID");
			var id = Console.ReadLine();
			try
			{
				var result = RequestAPI.Instanse.DeleteItem<bool>("/cars/car", id);
				if (result)
					Console.WriteLine("Car was removed succesfuly");
				else
					Console.WriteLine("Car didn`t find!");
			}
			catch (ArgumentNullException ex)
			{
				Console.WriteLine("Car didn`t find!");
				Logger.WriteException(ex.Message, "Parking.log");
				return;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Car wasn`t deleted!");
				Logger.WriteException(ex.Message, "Parking.log");
				return;
			}

		}

		public static void ShowAllCars()
		{
			Console.Clear();
			try
			{
				var result = RequestAPI.Instanse.GetItems<Car>("/cars/cars");
				if (result?.Count != 0)
				{
					foreach (var item in result)
					{
						Console.WriteLine(item.ToString());
					}
				}
				else
				{
				Console.WriteLine("Cars didn`t find!");
				}
			}
			catch (ArgumentNullException ex)
			{
				Console.WriteLine("Cars didn`t find!");
				Logger.WriteException(ex.Message, "Parking.log");
				return;
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
				Console.WriteLine("Cars didn`t find!");
				Logger.WriteException(ex.Message, "Parking.log");
				return;
			}
		}

		public static void AddMoney()
		{
			Console.Clear();
			Console.WriteLine("Enter car id");
			var id = Console.ReadLine();
			Console.WriteLine("Enter value of replenish");
			try
			{
				var money = Convert.ToDouble(Console.ReadLine());
				if (money < 0)
				{
					Console.WriteLine("You should enter positive number!");
					Thread.Sleep(1500);
					return;
				}

				RequestAPI.Instanse.PostItem<bool>("/cars/balance", new { ID = id, Balance = money });
			}
			catch (FormatException ex)
			{
				Console.WriteLine("Wrong format!");
				Logger.WriteException(ex.Message, "Parking.log");
				return;
			}
			catch (OverflowException ex)
			{
				Console.WriteLine("Too big number!");
				Logger.WriteException(ex.Message, "Parking.log");
				return;
			}
			catch (ArgumentNullException ex)
			{
				Console.WriteLine("Car didn`t find!");
				Logger.WriteException(ex.Message, "Parking.log");
				return;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Car didn`t find!");
				Logger.WriteException(ex.Message, "Parking.log");
				return;
			}
			Console.WriteLine("Money was added succesfuly");

		}

		public static void ShowHistory()
		{
			Console.Clear();
			var transactions = RequestAPI.Instanse.GetItems<Transaction>("/transactions/transactions");
			foreach (var transaction in transactions)
			{
				Console.WriteLine(transaction.ToString());
			}
		}

		public static void ShowBalance()
		{
			Console.Clear();
			Console.WriteLine($"Total income: {RequestAPI.Instanse.GetItem<double>("/parking/balance")}");
		}

		public static void ShowBalancePerMinute()
		{
			Console.Clear();
			Console.WriteLine($"Income for last minute: {RequestAPI.Instanse.GetItem<double>("/parking/income")}"); ;
		}

		public static void ShowPlaces()
		{
			Console.Clear();
			Console.WriteLine(RequestAPI.Instanse.GetItem<string>("/parking/places"));
		}

		public static void ShowLog()
		{
			Console.Clear();
			Console.WriteLine(RequestAPI.Instanse.GetItem<string>("/transactions/log"));
		}

		public static void ShowCar()
		{
			Console.Clear();
			Console.WriteLine("Enter car ID");
			var guid = Console.ReadLine();
			try
			{
				Console.WriteLine(RequestAPI.Instanse.GetItem<Car>("/cars/car", guid).ToString());
			}
			catch (ArgumentNullException ex)
			{
				Console.WriteLine("Car didn`t find!");
				Logger.WriteException(ex.Message, "Parking.log");
				return;
			}
			catch (NullReferenceException ex)
			{
				Console.WriteLine("Car didn`t find!");
				Logger.WriteException(ex.Message, "Parking.log");
				return;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Car didn`t find!");
				Logger.WriteException(ex.Message, "Parking.log");
				return;
			}
		}
		#endregion FirstMenuLevelMethods



		#region SecondMenuLevelMethods
		public static void Bus()
		{
			Console.Clear();
			Console.WriteLine("Enter balance");
			double balance;
			var result = EnterBalance(out balance);
			if (result)
			{
				var guid = RequestAPI.Instanse.PostItem<string>("/cars/car", new { Type = CarType.Bus, Balance = balance });
				Console.WriteLine("Car was added sucesfully");
				Console.WriteLine($"Car ID: {guid}");
			}
		}
		public static void Passenger()
		{
			Console.Clear();
			Console.WriteLine("Enter balance");
			double balance;
			var result = EnterBalance(out balance);
			if (result)
			{
				var guid = RequestAPI.Instanse.PostItem<string>("/cars/car", new { Type = CarType.Passenger, Balance = balance });
				Console.WriteLine("Car was added sucesfully");
				Console.WriteLine($"Car ID: {guid}");
			}
		}

		public static void Truck()
		{
			Console.Clear();
			Console.WriteLine("Enter balance");
			double balance;
			var result = EnterBalance(out balance);
			if (result)
			{
				var guid = RequestAPI.Instanse.PostItem<string>("/cars/car", new { Type = CarType.Truck, Balance = balance });
				Console.WriteLine("Car was added sucesfully");
				Console.WriteLine($"Car ID: {guid}");
			}
		}

		public static void Motorcycle()
		{
			Console.Clear();
			Console.WriteLine("Enter balance");
			double balance;
			var result = EnterBalance(out balance);
			if (result)
			{
				var guid = RequestAPI.Instanse.PostItem<string>("/cars/car", new { Type = CarType.Motorcycle, Balance = balance });
				Console.WriteLine("Car was added sucesfully");
				Console.WriteLine($"Car ID: {guid}");
			}
		}

		public static bool EnterBalance(out double balance)
		{
			balance = Convert.ToDouble(Console.ReadLine());
			if (balance < 0)
			{
				Console.WriteLine("Write positive number!");
				Thread.Sleep(1500);
				return false;
			}
			return true;
		}
		#endregion SecondMenuLevelMethods

	}
}
